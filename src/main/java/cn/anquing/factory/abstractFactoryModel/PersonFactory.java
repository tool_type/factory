package cn.anquing.factory.abstractFactoryModel;

public interface PersonFactory {

	Boy getBoy();
	
	Girl getGirl();
}
